FROM maven
EXPOSE 8761
RUN mkdir eureka
ADD . eureka
WORKDIR  eureka
RUN mvn clean install
ENTRYPOINT ["java","-jar","target/eureka-service-0.0.1-SNAPSHOT.jar"]